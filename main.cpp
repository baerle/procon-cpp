#include "Squares/squares.h"
#include "ThreeIntegers/threeintegers.h"
#include "PrimeSubstraction/primesubstraction.h"
#include "123-Sequence/123-sequence.h"
#include "ABitDifferent/aBitDifferent.h"
#include "Nineteen/nineteen.h"
#include "RightTriangles/rightTriangles.h"
#include "TwoIntegers/twoIntegers.h"
#include "MoreSquares/moreSquares.h"
#include "CargoFit/cargofit.h"
#include "ChristmasOrnament/ornament.h"
#include "DoggoRecolor/doggorecolor.h"

int main() {
    //squares();
    //threeintegers();
    //primesubstraction();
    //sequence123();
    //aBitDifferent();
    //nineteen();
    //rightTriangles();
    //twoIntegers();
    //moreSquares();
    //cargofit();
    //ornament();
    doggorecolor();

    return 0;
}
#include <iostream>

using namespace std;

int validate(int input[4]);
bool isQuadratic(const int input[4]);
bool isRectangle(const int input[4]);

int squares() {
    int nObj = {0};

    do {
        cin >> nObj;
    } while (nObj == 0);
    int geo[4] = {0};

    //Einlesen
    for (int i = 0; i < nObj; ++i) {
        //cout << i << endl;
        int input;
        for (int j = 0; j < 4; ++j) {
            cin >> input;
            geo[j] = input;
        }
        //cout << geo[0] << geo[1] << geo[2] << geo[3] << endl;
        cout << validate(geo) << endl;
    }

    return 0;
}

int validate(int input[4]) {
    if (isQuadratic(input)) {
        return 2;
    }
    if (isRectangle(input)) {
        return 1;
    }
    return 0;
    /*for (int i = 0; i < 4; i++) {
        cout << input[i] << endl;
    }*/
}

bool isQuadratic(const int input[4]) {
    if (input[0] == input [1] && input[0] == input[2] && input[0] == input[3]) {
        return true;
    }
    return false;
}

bool isRectangle(const int input[4]) {
    if ((input[0] == input[1] && input[2] == input[3])
        || (input[0] == input[3] && input[1] == input[2])
        || (input[0] == input[2] && input[1] == input[3]))
    {
            return true;
    }
    return false;
}
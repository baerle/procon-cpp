#include <iostream>

using namespace std;

bool areCharsDoubled(int &size, string &s);

int doggorecolor() {
    int n;
    string s;
    cin >> n;
    cin >> s;

    if (n < 2 || areCharsDoubled(n, s)) {
        cout << "Yes" << endl;
    } else {
        cout << "No" << endl;
    }

    return 0;
}

bool areCharsDoubled(int &size, string &s) {
    int chars[26];
    for (int &i : chars) {
        i = 0;
    }
    for (int i {0}; i < size; i++) {
        int pos = tolower(s[i]) - 'a';
        chars[pos]++;
        if (chars[pos] > 1) {
            return true;
        }
    }
    for (int c : chars) {
        if (c > 1) {
            return true;
        }
    }
    return false;
}
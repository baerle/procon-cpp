#include <iostream>

using namespace std;

int main() {
    int yellow, blue, red;

    cin >> yellow;
    cin >> blue;
    cin >> red;

    if (yellow < blue) {
        blue = yellow + 1;
    } else {
        yellow = blue - 1;
    }
    if (blue < red) {
        red = blue + 1;
    } else {
        blue = red - 1;
        yellow = blue - 1;
    }

    cout << (yellow + blue + red) << endl;

    return 0;
}
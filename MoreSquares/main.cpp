#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int nSides;
    cin >> nSides;

    int sides[nSides];
    for (int i = 0; i < nSides; ++i) {
        cin >> sides[i];
    }

    sort(sides, (sides + nSides));

    int nForSides[nSides][2];
    for (int i = 0; i < nSides; ++i) {
//        cout << sides[i] << " ";
        nForSides[i][0] = -1;
        nForSides[i][1] = -1;
    }
    //cout << endl;

    int max = -1;
    for (int i = 0; i < nSides; ++i) {
        bool set = false;
        for (int j = 0; j < nSides; ++j) {
            if (!set && nForSides[j][0] == sides[i]) {
                nForSides[j][1]++;
                set = true;
            } else if (!set && nForSides[j][0] == -1){
                nForSides[j][0] = sides[i];
                nForSides[j][1] = 1;
                max = j;
                set = true;
            }
        }
    }

    //cout << "max: " << max << endl;
    int nQuadrat {0};
    for (int i = 0; i <= max; ++i) {
        if (nForSides[i][1] >= 2) {
            for (int j = 0; j <= max; ++j) {
                if (nForSides[j][1] >= 2) {
                    if (i != j) {
                        nQuadrat++;
                        //cout << nForSides[i][0] << " : " << nForSides[j][0] << endl;
                    } else if (nForSides[i][1] >= 4) {
                        nQuadrat += 2;
                        //cout << "nQuadrat: " << nQuadrat << endl;
                    }
                }
            }
        }
        // cout << nForSides[i][1] << endl;
    }

    cout << (int) (nQuadrat/2) << endl;

    return 0;
}

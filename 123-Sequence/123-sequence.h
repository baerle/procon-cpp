#include <iostream>

using namespace std;

int sequence123() {
    int nNumbers = {0};

    do {
        cin >> nNumbers;
    } while (nNumbers == 0);
    int one = 0, two = 0, three = 0;

    //Einlesen
    for (int i = 0; i < nNumbers; ++i) {
        short input;
        cin >> input;
        switch (input) {
            case 1:
                one++;
                break;
            case 2:
                two++;
                break;
            case 3:
                three++;
                break;
            default:
                break;
        }
    }

    if (one <= three && two <= three) {
        cout << nNumbers - three << endl;
    } else if (one <= two && two >= three) {
        cout << nNumbers - two << endl;
    } else {
        cout << nNumbers - one << endl;
    }

    return 0;
}
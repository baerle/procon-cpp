#include <iostream>
#include <string>

int getMin(const int *characters);

using namespace std;

void nineteen() {
    int characters[4] = {0, 0, 0, 0};

    string input;
    cin >> input;

    for (char c : input) {
        switch(c) {
            case 'n':
                characters[0]++;
                break;
            case 'i':
                characters[1]++;
                break;
            case 'e':
                characters[2]++;
                break;
            case 't':
                characters[3]++;
                break;
        }
    }

    int min = getMin(characters);

    cout << min << endl;
}

int getMin(const int *characters) {
    int chars[4];
    for (int i = 0; i < 4; ++i) {
        chars[i] = characters[i];
        //cout << chars[i] << " ";
    }

    if (chars[0] > 3) {
        chars[0]--;
    }

    chars[0] /= 2;
    chars[2] /= 3;

    if (chars[0] < 2 && characters[0] < 3) {
        chars[0] = 0;
    }

    int min = {101};
    for (int i = 0; i < 4; i++) {
        if (chars[i] < min) {
            min = chars[i];
        }
    }
    return min;
}

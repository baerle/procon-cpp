#include <iostream>

using namespace std;

int main() {
    int N;
    cin >> N;

    for (int i = 0; i < N; ++i) {
        long a, b;
        cin >> a;
        cin >> b;

        uint diff = abs(a - b);

        uint result = diff / 10;
        if (diff % 10 != 0) result++;

        cout << result << endl;
    }

    return 0;
}
#include <iostream>

using namespace std;

int main() {
    int rows {};
    int cols {};

    cin >> rows;
    cin >> cols;

    char row[rows][cols];

    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            cin >> row[i][j];
            //cout << row[i][j];
        }
    }

    //cout << endl;
    int nTriangles {0};

    //check Triangle
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (row[i][j] == '*') {
                for (int k = 0; k < rows; ++k) {
                    if (k != i && row[k][j] == '*') {
                        for (int l = 0; l < cols; ++l) {
                            if (l != j && row[i][l] == '*') {
                                nTriangles++;
                            }
                        }
                    }
                }
            }
        }
    }

    cout << nTriangles << endl;

    return 0;
}

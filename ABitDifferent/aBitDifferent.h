#include <iostream>

using namespace std;

void aBitDifferent() {
    int nNumbers {0};

    cin >> nNumbers;

    int nums[nNumbers] {0};
    int nNums[nNumbers] {0};

    for (int i = 0; i < nNumbers; ++i) {
        int number {0};
        cin >> number;
        for (int j = 0; j < nNumbers; ++j) {
            if (nums[j] == number) {
                nNums[j]++;
                break;
            }
            if (nums[j] == 0) {
                nums[j] = number;
                nNums[j]++;
                break;
            }
        }
    }

    int max {0}, maxIndex {0};
    for (int i = 0; i < nNumbers; ++i) {
        if (nNums[i] > max) {
            //cout << nNums[i] << " > " << max << "    ";
            max = nNums[i];
            maxIndex = i;
            //cout << "maxIndex" << maxIndex;
        }
        //cout << nums[i] << ": " << nNums[i] << endl;
    }
    //cout << maxIndex << endl << endl;
    cout << nums[maxIndex] << endl;
}
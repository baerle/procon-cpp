#include <iostream>

using namespace std;

int main() {
    int nNumbers = {0};
    cin >> nNumbers;

    int nums[nNumbers];
    int nNums[nNumbers];

    for (int i = 0; i < nNumbers; ++i) {
        nums[i] = 0;
        nNums[i] = 0;
    }

    for (int i = 0; i < nNumbers; ++i) {
        int number = {0};
        cin >> number;
        for (int j = 0; j < nNumbers; ++j) {
            if (nums[j] == number) {
                nNums[j]++;
                break;
            }
            if (nums[j] == 0) {
                nums[j] = number;
                nNums[j]++;
                break;
            }
        }
    }

    int max = {0}, maxIndex = {0};
    for (int i = 0; i < nNumbers; ++i) {
        if (nNums[i] > max) {
            max = nNums[i];
            maxIndex = i;
        }
    }
    cout << nums[maxIndex] << endl;

    return 0;
}
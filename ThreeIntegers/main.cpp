#include <iostream>

using namespace std;

void printThreeSummands(long long sums[2]);

int main() {
    int nQueries = {0};

    do {
        cin >> nQueries;
    } while (nQueries == 0);
    long long geo[2] = {0};

    //Einlesen
    for (int i = 0; i < nQueries; ++i) {
        //cout << i << endl;
        long long input;
        for (int j = 0; j < 2; ++j) {
            cin >> input;
            geo[j] = input;
        }

        printThreeSummands(geo);
    }

    return 0;
}

void printThreeSummands(long long sums[2]) {
    long long *bigger, *smaller;
    if (sums[0] > sums[1]) {
        bigger = &sums[0];
        smaller = &sums[1];
    } else {
        smaller = &sums[0];
        bigger = &sums[1];
    }

    long long a = *bigger - *smaller + 1;
    long long b = *bigger - a;
    long long c = *smaller - b;

    if (b < c) {
        if (a < b) {
            cout << a << " " << b << " " << c << endl;
        } else {
            if (a < c) {
                cout << b << " " << a << " " << c << endl;
            } else {
                cout << b << " " << c << " " << a << endl;
            }
        }
    } else {    // c < b
        if (a < c) {
            cout << a << " " << c << " " << b << endl;
        } else {
            if (a < b) {
                cout << c << " " << a << " " << b << endl;
            } else {
                cout << c << " " << b << " " << a << endl;
            }
        }
    }
}
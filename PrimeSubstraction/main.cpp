#include <iostream>

using namespace std;

void printIsPrimeSubstractable(long long x, long long y);

bool isPrimeDividable(long long n, int prime);

bool isPrime(int num);

int main() {
    unsigned short nQueries = {0};

    do {
        cin >> nQueries;
    } while (nQueries == 0);
    long long geo[2] = {0};

    //Einlesen
    for (unsigned short i = 0; i < nQueries; ++i) {
        //cout << i << endl;
        long long input;
        for (int j = 0; j < 2; ++j) {
            cin >> input;
            geo[j] = input;
        }

        printIsPrimeSubstractable(geo[0], geo[1]);
    }

    return 0;
}

void printIsPrimeSubstractable(long long x, long long y) {
    long long diff = x - y;
    for (int i = 2; i <= diff; i += 2) {
        if (isPrime(i)) {
            if (isPrimeDividable(diff, i)) {
                cout << "YES" << endl;
                return;
            }
            if (i==2) i--;
        }
    }
    cout << "NO" << endl;
}

bool isPrimeDividable(long long n, int prime) {
    return n % prime == 0;
}

bool isPrime(int num) {
    for (int i = 2; i <= num / 2; ++i) {
        if (num % i == 0) {
            return false;
        }
    }
    return true;
}
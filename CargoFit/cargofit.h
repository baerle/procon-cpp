#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

vector<char> sizesSpaces;
vector<int> sizesContainer;

int checkContainerFitsSpace(int &co);
bool eraseSpaceAtPlace(int n);

bool sortAntiAlphabetical(char a, char b) {
    return (a > b);
}

void cargofit() {
    int nSpaces, nContainer;
    cin >> nSpaces;
    cin >> nContainer;
    sizesSpaces = vector<char>(nSpaces);
    sizesContainer = vector<int>(nContainer);

    for (int i = 0; i < nSpaces; ++i) {
        cin >> sizesSpaces[i];
    }

    for (int i = 0; i < nContainer; ++i) {
        cin >> sizesContainer[i];
    }

    if (nSpaces < nContainer) {
        cout << "No" << endl;
    } else  {
        sort(sizesSpaces.begin(), sizesSpaces.end(), sortAntiAlphabetical);
        for (int co : sizesContainer) {
            int x = checkContainerFitsSpace(co);
            if (!eraseSpaceAtPlace(x)) {
                cout << "No" << endl;
                return;
            }
        }
        cout << "Yes" << endl;
    }
}

int checkContainerFitsSpace(int& co) {
    for (int i = 0; i < sizesSpaces.size(); i++) {
        switch (sizesSpaces[i]) {
            case 'S':
                if (co <= 50) return i;
                break;
            case 'M':
                if (co <= 100) return i;
                break;
            case 'L':
                if (co <= 200) return i;
                break;
            default:
                return -1;
        }
    }
    return -1;
}

bool eraseSpaceAtPlace(int n) {
    if (n != -1) {
        sizesSpaces.erase(sizesSpaces.begin() + n);
        return true;
    }
    return false;
}